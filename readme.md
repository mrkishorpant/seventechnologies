Install Dependency
1-> git clone https://mrkishorpant@bitbucket.org/mrkishorpant/seventechnologies.git
2-> php composer.phar install
3-> php composer.phar dump-autoload
4-> Configuration Data within Environment file from environment.example


Once the above setup is done let configure the project code with DB Connection 

1-> Go to the configured folder
2-> sudo chmod -R 777 /seventechnologies/storage
3-> php artisan migrate




Create API Path

http://localhost/seventtechnologies/public/api/incident/create
Method : POST
------------------------------------------------------------------------
Sample Input JSON:
{
    "id": 0,
    "location": {
        "latitude": 12.9231501,
        "longitude": 74.7818517
    },
    "title": "Incident Title Testing",
    "category": 1,
    "people": [
        {
            "name": "Kishor Pant",
            "type": "staff"
        },
        {
            "name": "Kishor Chandra Pant",
            "type": "witness"
        },
        {
            "name": "Kishor Pant 01",
            "type": "staff"
        }
    ],
    "comments": "This is a string of comments",
    "incidentDate": "2020-09-01T13:26:00+00:00",
    "createDate": "2020-09-01T13:32:59+01:00",
    "modifyDate": "2020-09-01T13:32:59+01:00"
}
------------------------------------------------------------------------
Fetch API Path

Method : GET

http://local.seventech.com/api/incident/all
