<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncidentLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('incidentLocation')) {

            Schema::create('incidentLocation', function (Blueprint $table) {
                $table->bigIncrements('locationId');
                $table->integer('incidentId');
                $table->float('incidentLat');
                $table->float('incidentLong');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('incidentLocation');
    }
}