<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncidentPeoples extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('incidentPeoples')) {
            Schema::create('incidentPeoples', function (Blueprint $table) {
                $table->bigIncrements('incidentPeople');
                $table->integer('incidentId');
                $table->string('personName', 255);
                $table->string('personType', 20);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('incidentPeoples');
    }
}