<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Incident extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('incident')) {
            Schema::create('incident', function (Blueprint $table) {
                $table->bigIncrements('incidentId')->index('incidentId');
                $table->integer('incidentLocation');
                $table->string('incidentTitle', 255);
                $table->integer('incidentCategory');
                $table->dateTime('incidentDate');
                $table->text('incidentComment');
                $table->dateTime('incidentCreatedDate');
                $table->dateTime('incidentModifyDate');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('incident');
    }
}