<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Peoples extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('incidentPeoples')->insert(
            [9, 9, 'Name of person', 'staff'],
            [10, 9, 'Name of person', 'witness'],
            [11, 9, 'Name of person', 'witness'],
            [12, 10, 'Name of person', 'witness'],
            [13, 10, 'Name of person', 'witness']
        );
    }
}