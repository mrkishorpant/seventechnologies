<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Location extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('incidentLocation')->insert([
            9, 9, 12.92, 74.78
        ]);

        DB::table('incidentLocation')->insert([
            10, 10, 12.92, 74.78
        ]);
    }
}