<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Incident extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('incident')->insert([
            9, 'incident title', 1, '2020-09-01 13:26:00', 'This is a string of comments', '2020-09-01 12:32:59', '2020-09-01 12:32:59',
        ]);
        DB::table('incident')->insert([
            10, 'Incident Title Testing', 1, '2020-09-01 13:26:00', 'This is a string of comments', '2020-09-01 12:32:59', '2020-09-01 12:32:59',
        ]);
    }
}