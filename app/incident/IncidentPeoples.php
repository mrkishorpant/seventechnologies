<?php

namespace App\incident;

use Illuminate\Database\Eloquent\Model;

class IncidentPeoples extends Model
{
    //
    protected $table    =   'incidentPeoples';
    protected $primaryKey =  'incidentPeople';
    public $timestamps  =   false;
    protected $fillable   =   ['incidentId', 'personName', 'personType'];

    function toArray()
    {
        $data['name'] = $this->personName;
        $data['type'] = $this->personType;
        return $data;
    }
}