<?php

namespace App\incident;

use Illuminate\Database\Eloquent\Model;

class IncidentLocation extends Model
{
    //
    protected $table    =   'incidentLocation';
    protected $primaryKey =  'locationId';
    public $timestamps  =   false;
    protected $fillable   =   ['incidentId', 'incidentLat', 'incidentLong'];

    function toArray()
    {
        $data['latitude'] = number_format($this->incidentLat, 2);
        $data['longitude'] = number_format($this->incidentLong, 2);
        return $data;
    }
}