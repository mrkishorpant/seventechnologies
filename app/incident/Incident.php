<?php

namespace App\incident;

use Illuminate\Database\Eloquent\Model;
use App\incident\IncidentLocation as Location;
use App\incident\IncidentPeoples as Peoples;
use Illuminate\Support\Carbon;

class Incident extends Model
{
    //

    protected $table    =   'incident';
    protected $primaryKey =  'incidentId';
    public $timestamps  =   false;
    protected $fillable   =   [
        'incidentLocation', 'incidentTitle',
        'incidentCategory', 'incidentDate', 'incidentCreatedDate',
        'incidentComment',
        'incidentModifyDate'
    ];


    function toArray()
    {

        $data['id'] = $this->incidentId;
        $data['location'] = ($this->location);
        $data['title'] = $this->incidentTitle;
        $data['category'] = $this->incidentCategory;
        $data['people'] = $this->peoples;
        $data['comments'] = $this->incidentComment;
        $data['incidentdate'] = Carbon::parse($this->incidentDate);
        $data['createddate'] = Carbon::parse($this->incidentCreatedDate);
        $data['modifydate'] = Carbon::parse($this->incidentModifyDate);
        return $data;
    }
    function location()
    {
        return $this->belongsTo(Location::class, 'incidentId', 'incidentId');
    }

    function peoples()
    {
        return $this->hasMany(Peoples::class, 'incidentId', 'incidentId');
    }
}