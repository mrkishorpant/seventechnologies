<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\incident\IncidentLocation as Location;

use App\incident\Incident as Incidents;
use App\incident\IncidentPeoples;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class Incident extends Controller
{
    //

    function listIncident()
    {
        try {
            $Incident = Incidents::all();
            return response(['status' => true, 'data' => $Incident], 200);
        } catch (Exception $e) {
            return response(['response' => 500, 'error' => $e->getMessage()], 500);
        }
    }

    function createIncident(Request $request)
    {


        $Validation = Validator::make(($request->all()), [
            'location.latitude' => 'required|numeric|min:1',
            'location.longitude' => 'required|numeric',
            'location' => 'required',
            'category' => 'required|numeric|min:1',

        ]);

        if ($Validation->fails()) {
            return response(['error' => 201, 'response' => $Validation->errors()->first()]);
        }


        $newIncident = 0;

        $incident['incidentTitle'] = $request->title;
        $incident['incidentCategory'] = $request->category;
        $incident['incidentDate'] = Carbon::parse($request->incidentDate);
        $incident['incidentComment'] = $request->comments;
        $incident['incidentCreatedDate'] = $request->createDate;
        $incident['incidentModifyDate'] = $request->modifyDate;

        $location = $request->location;
        $peoples = $request->people;

        if (!empty($incident['incidentTitle'])) {
            $newIncident = Incidents::create($incident);
            $isLocationSaved = $this->saveLocation($location, $newIncident->incidentId);
            $isPeopleSaved = $this->savePeoples($peoples, $newIncident->incidentId);
        }
        $http['data'] = $newIncident;
        return response($http);
    }

    function saveLocation($location = null, $incidentId = null)
    {
        try {
            $locations['incidentLat'] = number_format($location['latitude'], 5);
            $locations['incidentLong'] = number_format($location['longitude'], 5);
            $locations['incidentId'] = $incidentId;
            Location::create($locations);
            return $locations;
        } catch (Exception $e) {
            return false;
        }
    }

    function savePeoples($peoples = null, $incidentId = null)
    {
        try {
            $peopleItem_master = [];
            foreach ($peoples as $peopleItem) {
                $peopleItem_master[] = ['incidentId' => $incidentId, 'personName' => $peopleItem['name'], 'personType' => $peopleItem['type']];
            }
            IncidentPeoples::insert($peopleItem_master);
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}